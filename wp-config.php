<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'awesomedesign' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '.I[SFf;(w.TRX;uy_Vm}[1+S8YzS$/6!8l-=Gwo9ndCykI_odV,4r`@(lO(v6K[H' );
define( 'SECURE_AUTH_KEY',  'fBs[aMZV,wPX;~dNF)~}|pbprjJL;C`B`Z T?j8Ymu6@]jSxM-pQcGDac]sP:<X/' );
define( 'LOGGED_IN_KEY',    'y*/YPNP zNnr$Im+Gc/h#l9sg^+I.:%vyYx1cl_YhEFHUBm@Y5M3}3l0?JZa.ERo' );
define( 'NONCE_KEY',        'vxuP;-:cKH8WFT+nk$tjpwrAU3(!+yH;$;6UG.6 Ge9d<oVz6%*.j0zvB]c r{2>' );
define( 'AUTH_SALT',        'FF{#qaloi>`*((^*J<]mcQ:)2wtO%#H7IdEtegvpy:;n=osq%t(Z*H()DH.g2G,-' );
define( 'SECURE_AUTH_SALT', ' +7luzt2_%pZh`B 6Kh*9oKM$2v%AJ[1i=2Rc=qtKu]a(7#cIUeXXlLPSD)q+bmZ' );
define( 'LOGGED_IN_SALT',   '_1cGlvl&C.)n@|1:E1(RydGm#cm?j3I h8riBV;#zPcHKa,SbF<wZ](g<D`wg.VH' );
define( 'NONCE_SALT',       'E0%(@Qso$(F,bL5ub5sdJAKkDzhmgIuly&piabO+HZ<bM:`0bMaSF{CSU>MAr(iT' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
