<?php
get_header();
?>

<?php do_shortcode('[getSlider]') ?>
    <section id="content">
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <?php $queried_object = get_queried_object();
                    if (get_field("about_content", $queried_object)) { ?>
                        <div id="about">
                            <h3 class="title-about">
                                About Us
                            </h3>
                            <p class="content-about">
                                <? echo get_field("about_content", $queried_object) ?>
                            </p>

                        </div>
                    <?php } ?>
                </div>
                <div class="col-xl-7 col-lg-8 offset-xl-1">
                    <div class="news">
                        <h3 class="title-news">
                            Our News
                        </h3>
                        <?php
                        $args = [
                            'post_type' => 'news',
                            'orderby' => 'date',
                            'order' => 'DESC',
                            'posts_per_page' => 2

                        ];
                        $query = new WP_Query($args);

                        if ($query->have_posts()) {

                            ?>
                            <div class="news-posts">
                                <?php
                                while ($query->have_posts()) {
                                    $query->the_post();
                                    ?>
                                    <a href="<?php the_permalink(); ?>" class="news-post">
                                        <div class="news-post-img">
                                            <?php
                                            if (has_post_thumbnail()) {
                                                the_post_thumbnail(array(196,107));
                                            } else {
                                                echo '<img width="196" height="107" src="/wp-content/uploads/2019/10/img-default.jpg" class="attachment-196x107 size-196x107 wp-post-image" alt="">';
                                            }
                                            ?>
                                        </div>
                                        <div class="news-post-content">
                                            <div class="news-post-date">
                                                <?= get_the_date("j, F Y") ?>
                                            </div>
                                            <div class="news-post-text">
                                                <?= get_the_content(); ?>
                                            </div>

                                        </div>

                                    </a>

                                <?php } ?>

                            </div>
                        <?php } ?>

                    </div>
                </div>
            </div>
            <?php echo do_shortcode('[testShortcode]'); ?>
        </div>

    </section>



<?php
get_footer();
