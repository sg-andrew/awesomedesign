<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php wp_title(); ?></title>
    <link rel="profile" href="http://gmpg.org/xfn/11"/>
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>"/>
    <?php if (is_singular() && get_option('thread_comments')) wp_enqueue_script('comment-reply'); ?>
    <?php wp_head(); ?>
</head>
<body>
<section id="header">
    <div class="cap-header">
        <div class="container">
            <div class="cap">
                <div class="row">
                    <div class="col-md-6">
                        <div class="cap-logo d-flex align-items-center">
                            <div class="logo">
                                <?php the_custom_logo(); ?>
                            </div>
                            <a href="<?= home_url(); ?>" class="site-title">
                                <?php bloginfo('name'); ?>
                            </a>
                        </div>

                    </div>
                    <div class="col-md-6">
                        <div class="phone d-flex align-items-center"><a href="tel:+3606906789">+360
                                690 67 89</a></div>
                    </div>

                </div>
            </div>

        </div>

    </div>
    <div class="container">
        <div class="nav-header">
            <div class="row">
                <div class="col-xl-3 col-lg-3 col-9">
                    <div class="search">
                        <?php dynamic_sidebar('search-widget'); ?>
                    </div>
                </div>

                <div class="col-xl-8 col-lg-9 col-3 offset-xl-1  d-flex justify-content-end">
                    <div class="menu_burger">
                        <div class="burger_item"></div>
                    </div>
                    <div class="header-menu">
                        <div class="close_burger_menu">
                            <div class="close_burger_menu_item"></div>
                        </div>
                        <?php wp_nav_menu([
                            'theme_location' => 'header',
                        ]); ?>
                    </div>

                </div>

            </div>


        </div>

    </div>


</section>