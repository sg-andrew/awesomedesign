
<footer>

    <div class="container">
        <div class="row">
            <div class="col-lg-4 d-flex offset-lg-4 justify-content-center align-items-center">
                <div class="logo">
                    <?php the_custom_logo(); ?>
                </div>
                <a href="<?= home_url(); ?>"  class="site-title">
                    <?php bloginfo('name'); ?>
                </a>
            </div>
        </div>
    </div>
</footer>
<?php wp_footer(); ?>
</body>
</html>