<?php
add_action( 'wp_enqueue_scripts', 'theme_style' );
add_action( 'wp_enqueue_scripts', 'theme_scripts' );
add_action( 'after_setup_theme', 'theme_menu' );
add_theme_support( 'post-thumbnails' );
add_theme_support( 'custom-logo' );
add_theme_support( 'title-tag' );
add_action('init', 'registerNewsPostType');
add_action( 'widgets_init', 'createWidgetZone' );



function theme_style() {
    wp_enqueue_style( 'style', get_stylesheet_uri() );
}

function theme_scripts(){
    wp_enqueue_script( 'script', get_template_directory_uri() . '/assets/js/scripts.js');
}
function theme_menu() {
    register_nav_menu( 'header', 'Header Menu' );
}

function registerNewsPostType()
{
    register_post_type('news', array(
        'labels' => array(
            'name' => 'News',
        ),
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
        'supports' => array('title', 'editor', 'thumbnail', )
    ));
}

function createWidgetZone() {
    register_sidebar( [
        'name' => 'Search widget',
        'id'   => "search-widget ",
    ]);

}


add_shortcode('testShortcode', 'createShortcode');

function createShortcode($attr, $content, $tag) {
    $contentShortcode = '<div class="test-shortcode">Hello Wordl!</div>';
    return  $contentShortcode;

}