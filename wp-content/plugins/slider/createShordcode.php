<?php
add_shortcode('getSlider', 'getSlider');
function getSlider()
{

    $args = [
        'post_type' => 'slider',
        'posp_per_page' => -1
    ];
    $query = new WP_Query($args);
    if ($query->have_posts()):
        ?>

        <section id="slider-section">
            <div class="slider-section container owl-carousel">
                <?php
                while ($query->have_posts()):
                    $query->the_post();

                    ?>

                        <div class="slider-item ">
                            <div class="container">
                            <div class="row">
                                <div class="col-lg-5 content-slide d-flex flex-column justify-content-between">
                                    <div class="post-date d-flex align-items-center">
                                        <img class="post-date-icon" src="http://awesomedesign/wp-content/uploads/2019/10/icon-date-slide.png" alt="icon-date"><?= get_the_date('j, F Y') ?>
                                    </div>
                                    <h2 class="title-slide"><?= get_post_meta(get_the_ID(), 'title', true) ?></h2>
                                    <a href="<?= get_post_meta(get_the_ID(), 'button_url', true) ?>"
                                       class="btn-custom btn-slide"><?= get_post_meta(get_the_ID(), 'button_label', true) ?></a>
                                </div>
                                <div class=" col-lg-7 thumbnail-slide">
                                    <?php

                                    if (has_post_thumbnail()) {
                                        the_post_thumbnail(array(710, 430));
                                    } else {
                                        echo '<img width="710" height="421" src="/wp-content/uploads/2019/10/img-default.jpg" class="attachment-710x430 size-710x430 wp-post-image" alt="" srcset="/wp-content/uploads/2019/10/img-default.jpg 736w, /wp-content/uploads/2019/10/img-default.jpg 300w" sizes="(max-width: 710px) 100vw, 710px">';

                                    }
                                    ?>
                                </div>

                            </div>
                        </div>
                    </div>
                <?php
                endwhile;
                ?>
            </div>

        </section>
    <?php endif;
}