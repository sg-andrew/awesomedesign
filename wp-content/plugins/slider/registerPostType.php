<?php
add_action( 'init', 'registerSliderPostType' );
function registerSliderPostType(){
	register_post_type('slider', array(
		'labels'             => array(
			'name'               => 'Slider',
			'menu_name'          => 'Slider'

		),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => true,
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array('title','thumbnail')
	) );
}
