<?php
add_action( 'add_meta_boxes', 'createSection' );
add_action( 'save_post', 'saveData' );

function createSection() {
	add_meta_box( 'slide', 'Data for slide', 'getSection', 'slider' );
}

function getSection( $post, $meta ) {

	wp_nonce_field( plugin_basename( __FILE__ ), 'slider' );


//	$date  = get_post_meta( $post->ID, 'date', true );
	$title      = get_post_meta( $post->ID, 'title', true );
	$buttonLabel  = get_post_meta( $post->ID, 'button_label', true );
	$buttonUrl  = get_post_meta( $post->ID, 'button_url', true );

	?>

    <div class="field">
<!--        <div class="field">-->
<!--            <label for="date">Date</label>-->
<!--            <input type="text" id="date" value="--><?//= $date; ?><!--" name="date">-->
<!--        </div>-->
        <label for="title">Title</label>
        <input type="text" id="title" value="<?= $title; ?>" name="title">
    </div>
    <div class="field">
        <label for="button-label">Button label</label>
        <input type="text" id="button-label" value="<?= $buttonLabel; ?>" name="button-label">
    </div>
    <div class="field">
        <label for="button-url">Button url</label>
        <input type="text" id="button-url" value="<?= $buttonUrl; ?>" name="button-url">
    </div>

	<?php
}


function saveData( $post_id ) {

	if ( ! isset( $_POST['slider'] ) )
		return;

	if ( ! wp_verify_nonce( $_POST['slider'], plugin_basename(__FILE__) ) )
		return;

	if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE )
		return;

	if( ! current_user_can( 'edit_post', $post_id ) )
		return;


//	$date      = sanitize_text_field( $_POST['date'] );
	$title      =  $_POST['title'];
	$buttonLabel  = sanitize_text_field( $_POST['button-label'] );
	$buttonUrl  = sanitize_text_field( $_POST['button-url'] );


//	update_post_meta( $post_id, 'date', $date );
	update_post_meta( $post_id, 'title', $title );
	update_post_meta( $post_id, 'button_label', $buttonLabel );
	update_post_meta( $post_id, 'button_url', $buttonUrl );
}