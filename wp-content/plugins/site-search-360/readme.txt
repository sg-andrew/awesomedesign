=== Site Search 360 ==

Contributors: dsky
Donate link: 
Tags: site search, wordpress search, search, better search, custom search, autocompletion, search suggest, autocomplete, suggest, typeahead, relevance search
Requires at least: 4.0.0
Tested up to: 5.2
Stable tag: 1.0.30
License: GPLv2
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Precise and fast search, autocompletion, and search suggestions for your WordPress page.

== Description ==

Site Search 360 replaces your standard WordPress search by a fast and precise on-site search on all your posts and pages. Site Search 360 is highly customizable and gives you [detailed insights](https://control.sitesearch360.com/) into search behavior.

Site Search 360 is responsive and mobile ready so your search will work no matter what screen your visitors are on.

[youtube https://www.youtube.com/watch?v=yZoYy-JBIh8]

## Features

* Fast indexing and swift search and suggestions
* Result set clusters: Group search results of the same type together, e.g. all article matches and all matches on review pages.
* Drop-in replacement: in most cases you do not need to change a single configuration to enable the search instantly.
* Fast typeahead autocomplete search suggestions based on titles, tags, and author names.
* Search results **automatically update** when you save, delete, or change search content.
* Easily customizable by CSS and the [dashboard](https://control.sitesearch360.com/).
* Site Search 360 [Search designer](https://www.sitesearch360.com/search-designer) compatible.

== Installation ==

1. Install the plugin.
2. Activate the plugin.
3. On the Configuration page, you log in to your existing Site Search 360 account or simply create a new one. You will get an email with credentials.
4. Optionally, configure the search in your WordPress backend. Indexing settings and analytics can be seen at your  [dashboard](https://control.sitesearch360.com/).

== Screenshots ==

1. An example of the grouped search suggest.
2. The main plugin dashboard.
3. An example of a configuration page.
4. An example of search results.
5. Analytics dashboard.

== Help ==
Need help? Just post your question in the [support forum](https://wordpress.org/support/plugin/site-search-360) or [chat with us](https://gitter.im/site-search-360/Lobby) right away.

== Changelog ==
= 1.0.30 =
* Add grid title configuration.

= 1.0.29 =
* Fix bug preventing menu saving.

= 1.0.28 =
* Create content group configurations when indexing with DB Mode.
* Improve labeling of uncategorized results.

= 1.0.27 =
* Empty the index after indexing type switch.
* Update image extraction.
* Logging out.
* Bug fix.

= 1.0.26 =
* Update reset password link.

= 1.0.25 =
* Bug fix.

= 1.0.24 =
* Bug fix.

= 1.0.23 =
* Add index synchronization settings.
* Add forgot password link.
* Add new options to configuration editor.

= 1.0.22 =
* Bug fix.

= 1.0.21 =
* Bug fix.

= 1.0.20 =
* Minor indexing change after login.
* Improve DB indexing mode.

= 1.0.19 =
* Bug fix.

= 1.0.18 =
* Check whether post should be indexed.

= 1.0.17 =
* Bug fix.

= 1.0.16 =
* Remove auto-reindex on login.

= 1.0.15 =
* Add crawler indexing switch.
* Improve WP indexing.

= 1.0.14 =
* Connect login page to the new login API.

= 1.0.13 =
* Add new configuration settings (masonry layout, image x title positioning, infinite scroll,...).
* Minor bug fixes.

= 1.0.12 =
* Ensure settings can be saved in the Site Search 360 control panel.
* Prevent creating duplicate filters.
* Add default 'Other' Content Group name.

= 1.0.11 =
* Bug fix.

= 1.0.10 =
* Bug fix.

= 1.0.9 =
* Warning bug fix.

= 1.0.8 =
* Indexing bug fix.

= 1.0.7 =
* Bug fix.

= 1.0.6 =
* Add custom post type indexing.
* Bug fix.

= 1.0.5 =
* Adjust some texts.

= 1.0.4 =
* Minor bug fix.

= 1.0.3 =
* Better WooCommerce support.

= 1.0.2 =
* Remove .mjs script file to avoid caching issues from other WP plugins.

= 1.0.1 =
* Minor bug fix.

= 1.0.0 =
* Full redesign.
* Update JavaScript plugin to v12 and load from Site Search 360 CDN.
* Add filter configuration.
* Add search designer connection.
* Change default indexing logic.
* Improve configuration editor.
* Split-up configuration pages.
* Add basic search statistics.
* Improved multisite support.
* Many small improvements and bug fixes.

= 0.7.1 =
* Admin panel checkboxes fix.

= 0.7.01 =
* Update JavaScript plugin to v11.48.

= 0.7.0 =
* Update SiteSearch360 JavaScript plugin to v11 - tabbed navigation, enhanced tracking, grid layout,... ([full changelog](https://docs.sitesearch360.com/en/release-notes)).
* Add editor mode.
* Add option to render search results using the theme template.

= 0.6.93 =
* Update to v10.56.

= 0.6.92 =
* Add Search Result URL configuration.

= 0.6.9 =
* Add group results configuration option and update SiteSearch JavaScript version to v10.49.

= 0.6.8 =
* Update SiteSearch360 JavaScript version to v10.24 - minor fixes and adjustments.

= 0.6.7 =
* Update SiteSearch360 JavaScript version to v10 - accessibility improvements, mobile suggestions, layover styling and layover search box.

= 0.6.6 =
* Update SiteSearch360 JavaScript version.

= 0.6.5 =
* Site id configuration.

= 0.6.1 =
* Fix page indexing/removing on save/trash.

= 0.6.0 =
* Additional configuration options.
* Update to latest version of Site Search 360 JavaScript plugin.

= 0.5.0 =
* Initial release.