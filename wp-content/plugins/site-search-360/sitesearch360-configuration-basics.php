<?php 
    $ss360_tracker = new SiteSearch360Tracker();
    $ss360_tracker->recordImpression('configuration-basic');
    
    $ss360_plugin = new SiteSearch360Plugin();
    $ss360_config = $ss360_plugin->getConfig();
    $ss360_is_full = $ss360_plugin->getType() == 'full';
    $ss360_update_flag = false;

    function updateConfigValue($ss360_configuration, $ss360_path1, $ss360_path2, $ss360_default, $ss360_case_insensitive){
        $ss360_post_key = $ss360_path1.'_'.$ss360_path2;
        if(isset($_POST[$ss360_post_key]) && (($ss360_case_insensitive && strtolower($_POST[$ss360_post_key])!=strtolower($ss360_default)) || ($_POST[$ss360_post_key]!=$ss360_default))){
            $ss360_configuration[$ss360_path1][$ss360_path2] = stripslashes($_POST[$ss360_post_key]);
        }else {
            unset($ss360_configuration[$ss360_path1][$ss360_path2]);
        }
        return $ss360_configuration;
    }

    if (!empty($_POST) && isset($_POST['_wpnonce'])) {
        $ss360_tracker->recordConfigChange('configuration-basic');
        if(!isset($ss360_config['searchBox'])){
            $ss360_config['searchBox'] = array();
        }
        $ss360_config['searchBox']['selector']  = isset($_POST['searchBox_selector']) ? stripslashes($_POST['searchBox_selector']) : '#searchBox, form[role="search"] input[name="s"]';
        $ss360_config = updateConfigValue($ss360_config, 'searchBox', 'searchButton', '', false);
        $ss360_config = updateConfigValue($ss360_config, 'searchBox', 'placeholder', '', false);
        if(empty($ss360_config['searchBox'])){
            unset($ss360_config['searchBox']);
        }
        
        if(!isset($ss360_config['style'])){
            $ss360_config['style'] = array();
        }
        $ss360_config = updateConfigValue($ss360_config, 'style', 'themeColor', '#1c5d7d', true);
        $ss360_config = updateConfigValue($ss360_config, 'style','loaderType', 'skeleton', true);
        if(isset($_POST['style_animationSpeed']) && intval($_POST['style_animationSpeed']) != 250){
            $ss360_config['style']['animationSpeed'] = intval($_POST['style_animationSpeed']);
        }else {
            unset($ss360_config['style']['animationSpeed']);
        }
        if(empty($ss360_config['style'])){
            unset($ss360_config['style']);
        }
        if(!isset($_POST['allowCookies']) || $_POST['allowCookies']!='on'){
            $ss360_config['allowCookies'] = false;
        }else {
            unset($ss360_config['allowCookies']);
        }

        if(!isset($ss360_config['tracking'])){
            $ss360_config['tracking'] = array();
        }
        $ss360_config['tracking']['providers'] = array();
        if(isset($_POST['tracking_providers_ga']) && $_POST['tracking_providers_ga']=='on'){
            $ss360_config['tracking']['providers'][] = 'GA';
        }
        if(isset($_POST['tracking_providers_gtm']) && $_POST['tracking_providers_gtm']=='on'){
            $ss360_config['tracking']['providers'][] = 'GTM';
        }
        if(empty($ss360_config['tracking']['providers'])){
            unset($ss360_config['tracking']['providers']);
        }
        if(isset($_POST['tracking_enhanced']) && $_POST['tracking_enhanced']=='on'){
            unset($ss360_config['tracking']['enhanced']);
        }else {
            $ss360_config['tracking']['enhanced'] = false;
        }
        if(empty($ss360_config['tracking'])){
            unset($ss360_config['tracking']);
        }
        
        if(!isset($ss360_config['voiceSearch'])){
            $ss360_config['voiceSearch'] = array();
        }
        if(isset($_POST['voiceSearch_enabled']) && $_POST['voiceSearch_enabled']=='on'){
            $ss360_config['voiceSearch']['enabled'] = true;
        }else {
            unset($ss360_config['voiceSearch']['enabled']);
        }
        $ss360_config = updateConfigValue($ss360_config, 'voiceSearch', 'lang', 'en-US', false);
        if(empty($ss360_config['voiceSearch'])){
            unset($ss360_config['voiceSearch']);
        }
        $ss360_plugin->saveConfig($ss360_config);
        $ss360_update_flag = true;
        update_option('ss360_config_modifications', ((int) get_option('ss360_config_modifications')) + 1);
    }

    // configuration entries
    $searchBox_selector = isset($ss360_config['searchBox']) && isset($ss360_config['searchBox']['selector']) ?  htmlspecialchars($ss360_config['searchBox']['selector']) : '#searchBox';
    $searchBox_searchButton = isset($ss360_config['searchBox']) && isset($ss360_config['searchBox']['searchButton']) ? htmlspecialchars($ss360_config['searchBox']['searchButton']) : '';
    $searchBox_placeholder = isset($ss360_config['searchBox']) && isset($ss360_config['searchBox']['placeholder']) ? htmlspecialchars($ss360_config['searchBox']['placeholder']) : '';

    $style_themeColor = isset($ss360_config['style']) && isset($ss360_config['style']['themeColor']) ? $ss360_config['style']['themeColor'] : '#1c5d7d';
    $style_loaderType = isset($ss360_config['style']) && isset($ss360_config['style']['loaderType']) ? $ss360_config['style']['loaderType'] : 'skeleton'; 
    $style_animationSpeed = isset($ss360_config['style']) && isset($ss360_config['style']['animationSpeed']) ? $ss360_config['style']['animationSpeed'] : 250;

    $ss360_allowCookies = isset($ss360_config['allowCookies']) ? $ss360_config['allowCookies'] : true;
    if(!isset($ss360_config['tracking'])){
        $ss360_config['tracking'] = array();
    }
    if(!isset($ss360_config['tracking']['providers']) || !is_array($ss360_config['tracking']['providers'])){
        $ss360_config['tracking']['providers'] = array();
    }
    $tracking_providers_gtm =  in_array('GTM', $ss360_config['tracking']['providers']);
    $tracking_providers_ga = in_array('GA', $ss360_config['tracking']['providers']);
    $tracking_enhanced = isset($ss360_config['tracking']['enhanced']) ? $ss360_config['tracking']['enhanced'] : true;
    $voiceSearch_enabled = isset($ss360_config['voiceSearch']) && isset($ss360_config['voiceSearch']['enabled']) ? $ss360_config['voiceSearch']['enabled'] : false;
    $voiceSearch_lang = isset($ss360_config['voiceSearch']) && isset($ss360_config['voiceSearch']['lang']) ? $ss360_config['voiceSearch']['lang'] : 'en-US';
    

    // helpers
    $ss360_animationDuration = $style_animationSpeed == 0 ? 0 : ((1000 + $style_animationSpeed)/1000) . 's'; 
    $ss360_langs = array(
        array("cs", "Czech"),
        array("da", "Danish"),
        array("nl", "Dutch"),
        array("en-gb", "English (UK)"),
        array("en-US", "English (US)"),
        array("fi", "Finnish"),
        array("de", "German"),
        array("el", "Greek"),
        array("hu", "Hungarian"),
        array("it", "Italian"),
        array("lv", "Latvian"),
        array("lt", "Lithuanian"),
        array("no", "Norwegian"),
        array("pt", "Portugese"),
        array("ro", "Romanian"),
        array("ru", "Russian"),
        array("es", "Spanish"),
        array("sv", "Swedish"),
        array("tr", "Turkish")
    )
?>

<section id="ss360" class="wrap wrap--blocky flex flex--column flex--center">
    <?php 
        if($ss360_update_flag){ ?>
            <section class="wrapper wrapper--narrow bg-g message">
                <div class="block block--first flex">
                    <span><?php esc_html_e('The configuration has been saved.', 'site-search-360'); ?></span>
                    <button class="button button--close message__close" aria-label="<?php esc_html_e('Close', 'site-search-360'); ?>">&times;</button>
                </div>
            </section>
       <?php }
    ?>
    <section class="wrapper wrapper--narrow">
        <form class="block block--first" method="post" name="ss360_basic_config" action="<?php esc_url($_SERVER['REQUEST_URI'])?>">
            <?php wp_nonce_field(); ?>
            <h2><?php esc_html_e('Basic Configuration', 'site-search-360'); ?></h2>
            <section>
                <h3 class="m-b-0 c-b"><?php esc_html_e('Search Field', 'site-search-360') ?></h3>
                <table class="configuration">
                    <tbody>
                        <tr>
                            <td><strong><label for="searchBox--selector"><?php esc_html_e('Search Field Selector', 'site-search-360') ?></label></strong></td>
                            <td><input id="searchBox--selector" name="searchBox.selector" type="text" placeholder="#searchBox" class="input input--inline" value="<?php echo $searchBox_selector; ?>"></td>
                            <td><?php esc_html_e('The CSS selector to the search input.', 'site-search-360'); ?></td>
                        </tr>
                        <tr <?php echo $ss360_is_full ? '': 'style="display:none;"' ?>>
                            <td><strong><label for="searchBox--searchButton"><?php esc_html_e('Search Button Selector', 'site-search-360') ?></label></strong></td>
                            <td><input id="searchBox--searchButton" name="searchBox.searchButton" type="text" placeholder="#searchButton" class="input input--inline" value="<?php echo $searchBox_searchButton; ?>"></td>
                            <td><?php esc_html_e('The CSS selector to the search button (optional).', 'site-search-360'); ?></td>
                        </tr>
                        <tr>
                            <td><strong><label for="searchBox--placeholder"><?php esc_html_e('Search Field Placeholder', 'site-search-360') ?></label></strong></td>
                            <td><input id="searchBox--placeholder" name="searchBox.placeholder" type="text" placeholder="<?php esc_html_e('Search', 'site-search-360') ?>" class="input input--inline" value="<?php echo $searchBox_placeholder ?>"></td>
                            <td><?php esc_html_e('The placeholder of the search input (will only be shown if no placeholder is defined in the HTML Markup).', 'site-search-360'); ?></td>
                        </tr>
                    </tbody>
                </table>
            </section>
            <hr/>
            <section>
                <h3 class="m-b-0 c-b"><?php esc_html_e('Style', 'site-search-360') ?></h3>
                <table class="configuration">
                    <tbody>
                        <tr>
                            <td><strong><label for="style--themeColor"><?php esc_html_e('Theme Color', 'site-search-360') ?></label></strong></td>
                            <td class="flex"><input id="style--themeColor" class="fake-hide" type="color" value="<?php echo $style_themeColor?>" name="style.themeColor"><div id="color--p" class="input input--inline" style="width:37px;height:37px;background:<?php echo $style_themeColor?>;cursor:pointer;"></div><input id="color--i" class="input input--inline" type="text" value="<?php echo $style_themeColor?>" placeholder="#1C5D7D" pattern="(^#[0-9A-Fa-f]{6}$)|(^#[0-9A-Fa-f]{3}$)"></td>
                            <td><?php esc_html_e('The theme color (used for buttons, loaders, etc.).', 'site-search-360'); ?></td>
                        </tr>
                        <tr <?php echo $ss360_is_full ? '': 'style="display:none;"' ?>>
                            <td><strong><?php esc_html_e('Loading Animation', 'site-search-360') ?></strong></td>
                            <td class="flex">
                                <label class="radio m-r-1" aria-label="<?php esc_html_e('Skeleton', 'site-search-360') ?>">
                                    Skeleton
                                    <input id="loading-circle--i" type="radio" name="style.loaderType" <?php echo $style_loaderType == 'skeleton' ? 'checked' : '' ?> value="skeleton" class="fake-hide">
                                    <span class="radio_checkmark"></span>
                                </label>
                                <label class="radio" aria-label="<?php esc_html_e('Pulsating Circle', 'site-search-360') ?>">
                                    <div class="ss360-spinner-circle ss360-loader" style="width:25px; height:25px;">
                                        <div class="ss360-double-bounce1 colorize animate" style="background:<?php echo $style_themeColor?>;animation-duration:<?php echo $ss360_animationDuration;?>"></div>
                                        <div class="ss360-double-bounce2 colorize animate" style="background:<?php echo $style_themeColor?>;animation-duration:<?php echo $ss360_animationDuration;?>"></div>
                                    </div>
                                    <input id="loading-circle--i" type="radio" name="style.loaderType" <?php echo $style_loaderType == 'circle' ? 'checked' : '' ?> value="circle" class="fake-hide">
                                    <span class="radio_checkmark"></span>
                                </label>
                                <label class="radio m-l-1" aria-label="<?php esc_html_e('Flipping Square', 'site-search-360') ?>">
                                    <div class="ss360-loader ss360-spinner-square colorize animate" style="width:25px; height:25px;background:<?php echo $style_themeColor?>;animation-duration:<?php echo $ss360_animationDuration;?>"></div>
                                    <input id="loading-square--i" type="radio" class="fake-hide" name="style.loaderType" value="square" <?php echo $style_loaderType == 'square' ? 'checked' : '' ?>>
                                    <span class="radio_checkmark"></span>
                                </label>
                            </td>
                            <td><?php esc_html_e('The animation to show when search results are loading (by default a grey skeleton screen - outlining the future search layout - will be shown).', 'site-search-360')?></td>
                        </tr>
                        <tr>
                            <td><strong><label for="style--animationSpeed"><?php esc_html_e('Animation Speed', 'site-search-360') ?></label></strong></td>
                            <td><input value="<?php echo $style_animationSpeed; ?>" id="style--animationSpeed" name="style.animationSpeed" min="0" max="1000" step="50" type="number" placeholder="250" class="input input--inline"></td>
                            <td><?php esc_html_e('The animation duration in milliseconds (0: no animation; 1: fast animation; 1000: slow animation).', 'site-search-360'); ?></td>
                        </tr>
                    </tbody>
                </table>
            </section>
            <hr/>
            <section>
                <h3 class="m-b-0 c-b"><?php esc_html_e('Other', 'site-search-360') ?></h3>
                <table class="configuration">
                    <tbody>
                        <tr>
                            <td><strong><label for="allowCookies"><?php esc_html_e('Cookies', 'site-search-360') ?></label></strong></td>
                            <td><label class="checkbox"><?php esc_html_e('enabled', 'site-search-360') ?><input class="fake-hide" type="checkbox" id="allowCookies" name="allowCookies" <?php echo $ss360_allowCookies ? 'checked':''?>/><span class="checkbox_checkmark"></span></label></td>
                            <td><?php esc_html_e('Whether to allow Site Search 360 to create cookies.', 'site-search-360'); ?></td>
                        </tr>
                        <tr>
                            <td><strong><?php esc_html_e('Tracking', 'site-search-360') ?></strong></td>
                            <td class="flex" style="flex-wrap: wrap;padding-top:2em;">
                                <label <?php echo $ss360_is_full ? '': 'style="display:none;"' ?> class="checkbox p-b-0-5 p-r-0-5"><?php esc_html_e('Google Tag Manager', 'site-search-360') ?><input class="fake-hide" type="checkbox" name="tracking.providers.gtm" <?php echo $tracking_providers_gtm ? 'checked' : ''?>/><span class="checkbox_checkmark"></span></label>
                                <label <?php echo $ss360_is_full ? '': 'style="display:none;"' ?> class="checkbox p-b-0-5 p-r-0-5"><?php esc_html_e('Google Analytics', 'site-search-360') ?><input class="fake-hide" type="checkbox" name="tracking.providers.ga" <?php echo $tracking_providers_ga ? 'checked' : ''?>/><span class="checkbox_checkmark"></span></label>
                                <label class="checkbox p-b-0-5 p-r-0-5"><?php esc_html_e('Site Search 360 enhanced', 'site-search-360') ?><input class="fake-hide" type="checkbox" name="tracking.enhanced" <?php echo $tracking_enhanced ? 'checked' : ''?>/><span class="checkbox_checkmark"></span></label>
                            </td>
                            <td><?php esc_html_e('Which tracking providers to use to track search interactions.', 'site-search-360'); ?></td>
                        </tr>
                        <tr <?php echo $ss360_is_full ? '': 'style="display:none;"' ?>>
                            <td><strong><label for="voiceSearch-enabled"><?php esc_html_e('Voice Search', 'site-search-360') ?></label></strong></td>
                            <td>
                                <label class="checkbox"><?php esc_html_e('enabled', 'site-search-360') ?><input id="voiceSearch-enabled" class="fake-hide" type="checkbox" name="voiceSearch.enabled" <?php echo $voiceSearch_enabled ? 'checked' : ''?>/><span class="checkbox_checkmark"></span></label>
                            </td>
                            <td><?php esc_html_e('Whether to add the voice search option (if enabled, a microphone icon will be automatically added to your search field, only available for certain browsers).', 'site-search-360'); ?></td>
                        </tr>
                        <tr class="voicesearch-lang" <?php echo $voiceSearch_enabled && $ss360_is_full ? '' : 'style="display:none;"'?>>
                            <td><strong><label for="voiceSearch--lang"><?php esc_html_e('Voice Search Language', 'site-search-360') ?></label></strong></td>
                            <td>
                                <select id="voiceSearch--lang" name="voiceSearch.lang" class="input select m-b-0" value="<?php echo $voiceSearch_lang; ?>">
                                    <?php foreach($ss360_langs as $ss360_lang){ ?>
                                        <option value="<?php echo $ss360_lang[0]; ?>" <?php echo $ss360_lang[0] == $voiceSearch_lang ? 'selected' : ''?>><?php echo $ss360_lang[1]; ?></option>
                                    <?php } ?>
                                </select>
                            </td>
                            <td><?php esc_html_e('The language of the voice search.', 'site-search-360'); ?></td>
                        </tr>
                    </tbody>
                </table>
            </section>
            <div class="flex flex--center w-100 m-t-1">
                <button class="button button--padded" type="submit"><?php esc_html_e('Save', 'site-search-360'); ?></button>
            </div>
        </div>
    </section>
</section>


<script type="text/javascript">
(function(){
    // colors
    var hiddenColor = jQuery("#style--themeColor");
    var color = jQuery("#color--i");
    var preview = jQuery("#color--p");
    var colorize = jQuery(".colorize");
    preview.on("click", function(){
        hiddenColor.click();
    })
    hiddenColor.on("change", function(e){
        color.val(e.target.value);
        preview.css("background", e.target.value);
        colorize.css("background", e.target.value);    
    });
    color.on("keyup", function(e){
        var color = e.target.value;
        if(color.indexOf("#")!==0){
            color = "#" + color;
        }
        var isValidInput  = /(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)/i.test(color);
        if(isValidInput){
            if(color.length===4){
                color = "#" + color[1] + color[1] + color[2] + color[2] + color[3] + color[3];
            }
            hiddenColor.val(color);
            preview.css("background", color);
            colorize.css("background", color);
        }
    });

    // animations
    var animations = jQuery(".animate");
    jQuery("#style--animationSpeed").on("change", function(e){
        var animationSpeed = 1000 + parseInt(e.target.value);
        if(animationSpeed === 1000){
            animationSpeed = 0;
        }
        animations.css("animation-duration", (animationSpeed/1000) + 's');
    });

    // voice search
    var voiceSearchLang = jQuery(".voicesearch-lang");
    jQuery("#voiceSearch-enabled").on("change", function(e){
        if(e.target.checked){
            voiceSearchLang.show();
        }else {
            voiceSearchLang.hide();
        }
    });
    
    jQuery(".message__close").on("click", function(e){
        jQuery(e.target).parents(".message").fadeOut();
    });
}());
</script>