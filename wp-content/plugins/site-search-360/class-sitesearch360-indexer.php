<?php 
    class SiteSearch360Indexer {
        
        private $content_type = NULL;
        private $offset = NULL;
        private $count = NULL;
        private $client = NULL;
        private $category_filter_id = NULL;
        private $tag_filter_id = NULL;
        private $defined_data_points = NULL;
        private $data_point_names = NULL;
        private $data_points_inactive = NULL;

        public function __construct($content_type, $offset, $count) {
            $this->content_type = $content_type;
            $this->offset = $offset;
            $this->count = $count;
            $this->client = new SiteSearch360Client();
            $this->category_filter_id = get_option("ss360_category_filter_id");
            $this->tag_filter_id = get_option("ss360_tag_filter_id"); 
            if($this->category_filter_id==NULL){
                $this->category_filter_id = $this->client->createFilter(esc_html__('Category','site-search-360'), 'COLLECTION', 'OR');
                update_option("ss360_category_filter_id", $this->category_filter_id);
            }
            if($this->tag_filter_id==NULL){
                $this->tag_filter_id = $this->client->createFilter(esc_html__('Tag','site-search-360'), 'COLLECTION', 'OR');
                update_option("ss360_tag_filter_id", $this->tag_filter_id);
            }
            $this->defined_data_points = get_option('ss360_data_points');
            $this->data_point_names = get_option('ss360_renamed_dp');
            $this->data_points_inactive = get_option('ss360_inactive_dp');
            if($this->defined_data_points==null){
                $this->defined_data_points = array();
            }
            if($this->data_point_names==null){
                $this->data_point_names = array();
            }
            if($this->data_points_inactive==null){
                $this->data_points_inactive = array();
            }
            $ss360_indexing_mode = get_option('ss360_indexing_mode');
            if($ss360_indexing_mode=='db' || $ss360_indexing_mode==FALSE || $ss360_indexing_mode==NULL){
                $date_key = esc_html__('Date', 'site-search-360');
                if(isset($this->data_point_names[$date_key])){
                    $this->client->ensureSortDataPointExists($this->data_point_names[$date_key]);
                }
                $this->client->ensureContentGroupsExist(get_categories());
            }
            
        }    

        public function index(){
            $posts = get_posts(array(
                'posts_per_page' => $this->count,
                'offset' => $this->offset,
                'orderby' => 'date',
                'order' => 'ASC',
                'post_type' => $this->content_type,
                'post_status' => 'publish'
            ));
            $pages = array();
            foreach($posts as $post){
                $pages[] = $this->preparePage($post);
            }
            $this->client->bulkIndex($pages);
        }

        public function indexSingle($postId){
            $ss360_indexing_mode = get_option('ss360_indexing_mode');
            if($ss360_indexing_mode==null || $ss360_indexing_mode==FALSE){
                $ss360_indexing_mode = 'db';
            }
            $post = get_post($postId);
            if($ss360_indexing_mode == 'db'){
                $this->content_type = $post->post_type;
                $ss360_plugin = new SiteSearch360Plugin();
                $ss360_post_types = $ss360_plugin->getPostTypes();
                if($ss360_post_types!=NULL && $post->post_type !== NULL && array_search($post->post_type, $ss360_post_types)!==FALSE){
                    $page = $this->preparePage($post);
                    $this->client->indexPage($page);
                }
            } else {
                $this->client->notifyCrawler(get_permalink($post));
            }
        }

        private function isDataPointActive($key){
            return !in_array($key, $this->data_points_inactive);
        }

        private function preparePage($post){
            $excerpt = $post->post_excerpt;
            $content = strip_shortcodes(wp_filter_nohtml_kses($post->post_content));

            $publish_date = $post->post_date;
            $categories = get_the_category($post->ID);
            $author = get_the_author_meta('nickname', $post->post_author);
            $tags = wp_get_post_tags($post->ID);

            $page = array();
            // basic information
            $page['url'] = get_permalink($post);
            $page['title'] = $post->post_title;
            $page['snippet'] = $excerpt ? strip_shortcodes(wp_filter_nohtml_kses($excerpt)) : substr($content, 0, 350);
            $page['content'] = $content;
            $imageUrl = get_the_post_thumbnail_url($post);
            if($imageUrl!==FALSE){
                $page['imageUrl'] = $imageUrl;
            }

            // data points and content group
            $structuredData = array();
            $author_key = esc_html__('Author', 'site-search-360');
            $publish_date_key = esc_html__('Publication Date', 'site-search-360');
            $date_key = esc_html__('Date', 'site-search-360');
            
            $dp_needs_update = false;
            if(!in_array($author_key, $this->defined_data_points)){
                $this->defined_data_points[] = $author_key;
                $dp_needs_update = true;
            }
            if(!in_array($publish_date_key, $this->defined_data_points)){
                $this->defined_data_points[] = $publish_date_key;
                $dp_needs_update = true;
            }
            if(!in_array($date_key, $this->defined_data_points)){
                $this->defined_data_points[] = $date_key;
                $dp_needs_update = true;
            }

            if($this->isDataPointActive($author_key)){
                $structuredData[] = $this->createDataPoint($author_key, $author);
            }
            if($this->isDataPointActive($publish_date_key)){ 
                $structuredData[] = $this->createDataPoint($publish_date_key, $publish_date);
            }
            if($this->isDataPointActive($date_key)){//TODO: make sure global sort data point exists
                $structuredData[] = $this->createDataPoint($date_key, strtotime($publish_date), false, true);
            }
            $page['contentGroup'] = $this->getTopCategory($categories);
            
            // filters
            $filters = array();
            $tag_filter_values = array();
            $category_filter_values = array();
            foreach($tags as $tag){
                $tag_filter_values[] = $tag->name;
            }
            foreach($categories as $category){
                $category_filter_values[] = $category->name;
            }
            $filters[] = $this->createFilter($this->tag_filter_id, $tag_filter_values);
            $filters[] = $this->createFilter($this->category_filter_id, $category_filter_values);

            if($this->content_type == 'product'){ 
                $product_meta = get_post_meta($post->ID, '_product_attributes');
                $product_price = get_post_meta($post->ID, '_price');
                if(sizeof($product_price) > 0){ // Price filter
                    $price_filter_id = get_option("ss360_price_filter_id");
                    if($price_filter_id==null){
                        $price_filter_id = $this->client->createFilter(esc_html__('Price','site-search-360'), 'SINGLE_NUMERIC', '');
                        update_option("ss360_price_filter_id", $price_filter_id);
                    }
                    $filters[] = $this->createFilter($price_filter_id, $product_price[0]);
                    $price_key = esc_html__('Price', 'site-search-360');
                    if($this->isDataPointActive($price_key)){
                        $structuredData[] = $this->createDataPoint($price_key, $product_price[0]);
                    }
                    if(!in_array($price_key, $this->defined_data_points)){
                        $this->defined_data_points[] = $price_key;
                        $dp_needs_update = true;
                    }
                }            
                if(sizeof($product_meta) > 0){ // Attribute filters
                    $keys = array_keys($product_meta[0]);
                    foreach($keys as $key){
                        $filter_definition = $product_meta[0][$key];
                        if($filter_definition['is_visible']){
                            $filter_db_key = "ss360_".$key."_customfilter_id";
                            $filter_id = get_option($filter_db_key);
                            $filter_name = $filter_definition['name'];
                            if(sizeof($filter_name)==0){
                                continue;
                            }
                            if($filterId==null){
                                $filter_id = $this->client->createFilter($filter_name, 'COLLECTION', 'OR');
                                update_option($filter_db_key, $filter_id);
                            }
                            $filter_vals =  explode('|',$filter_definition['value']);
                            $filters[] = $this->createFilter($filter_id, $filter_vals);
                            if(!in_array($filter_name, $this->defined_data_points)){
                                $this->defined_data_points[] = $filter_name;
                                $dp_needs_update = true;
                            }
                            if($this->isDataPointActive($filter_name)){
                                foreach($filter_vals as $filter_val){
                                    $structuredData[] = $this->createDataPoint($filter_name, $filter_val);
                                }
                            }   
                        }                    
                    }
                }
            }

            if($dp_needs_update){
                update_option('ss360_data_points', $this->defined_data_points);
            }

            $page['structuredData'] = $structuredData; 
            $page['filters'] = $filters;

            return $page;
        }

        private function getTopCategory($categories){
            $category_by_parent = array();
            foreach($categories as $category){ // try to find top level category
                $parentCategoryId = $category->category_parent;
                if($parentCategoryId==0){
                    return $category->name;
                }
                $categoryId = $category->cat_ID;
                $category_by_parent[$parentCategoryId] = $categoryId; //TODO: might be an array (for future reference, but right now sufficient)
            }
            foreach($categories as $category){ // try to find category which does not have any parent among the categories
                if(!isset($category_by_parent[$category->category_parent])){
                    return $category->name;
                }
            }
            return "_";
        }

        private function createFilter($key, $values){
            $filter = array();
            $filter['key'] = $key;
            $filter['value'] = $values;
            return $filter;
        }

        private function createDataPoint($key, $value, $show=true, $sort=false){
            $dataPoint = array();
            $dataPoint['key'] = $this->data_point_names[$key]!=null ? $this->data_point_names[$key] : $key;
            $dataPoint['show'] = $show;
            $dataPoint['value'] = $value;
            $dataPoint['boost'] = false;
            $dataPoint['sort'] = $sort;
            $dataPoint['xpath'] = '//noxpath';
            if($sort){
                $dataPoint['single'] = true;
            }
            return $dataPoint;
        }
    }
?>