<?php

/**
 * Site Search 360 Wordpress Plugin
 *
 * This class takes care of the communication between Wordpress and the Site Search 360 API.
 *
 * @author David Urbansky <david@sitesearch360.com>
 *
 * @since 1.0
 *
 */
class SiteSearch360Plugin
{

    private $client = NULL;
    private $document_type_slug = 'posts';
    private $excluded_post_types = array('scheduled-action', 'nav_menu_item');

    public function __construct()
    {
        $this->client = new SiteSearch360Client();

        add_action('admin_menu', array($this, 'sitesearch360Menu'));
        add_action('admin_init', array($this, 'initializeAdminPage'));

        // hooks for sending post updates to the Site Search 360 API
        $syncOnSave = get_option('ss360_sync_on_save');
        $syncOnStatus = get_option('ss360_sync_on_status');
        $syncOnFuture = get_option('ss360_sync_on_future');
        $syncOnDelete = get_option('ss360_sync_on_delete');
        if($syncOnFuture==null || $syncOnFuture){
            add_action('future_to_publish', array($this, 'handleFutureToPublish'));
        }
        if($syncOnSave==null || $syncOnSave){
            add_action('save_post', array($this, 'handleSavePost'), 99, 1);
        }
        if($syncOnStatus==null || $syncOnStatus){
            add_action('transition_post_status', array($this, 'handleTransitionPostStatus'), 99, 3);
        }
        if($syncOnDelete==null || $syncOnDelete){
            add_action('trashed_post', array($this, 'deletePost'));
        }

        if($this->getType()!="filter"){
            add_action('wp_enqueue_scripts', array($this, 'enqueueSitesearch360Assets'));
        }
        add_action('admin_enqueue_scripts', array($this, 'enqueue_admin_assets'));
        add_action('wp_ajax_ss360_index', array($this, 'sitesearch360Index'));
        add_action('wp_ajax_ss360_track', array($this, 'sitesearch360Track'));
        add_action('plugins_loaded', array($this, 'sitesearch360CheckVersion'));
        if(get_option('ss360_review_interaction')==null){
            add_action('wp_ajax_ss360_review', array($this, 'sitesearch360ReviewInteracted'));
        }
        // override wordpress default search engine
        if($this->getType()!="full"){
            add_filter('the_posts', array( $this, 'overrideSearch' ), 99, 2 );
        }
    }

    /**
     * Initialize the Site Search 360 Search plugin's admin screen
     */
    public function initializeAdminPage()
    {
        load_plugin_textdomain( 'site-search-360', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' ); // load localized texts
    }

    public function createInitialConfig($siteId, $searchBox, $searchButton, $hasContentGroups){
        $configuration = array();
        $configuration['siteId'] = $siteId;
        $configuration['showErrors'] = false;

        $configuration['searchBox'] = array();
        $configuration['searchBox']['selector'] = $searchBox!=null ?  $searchBox : '#searchBox, form[role="search"] input[name="s"]';
        if($searchButton!=null){
            $configuration['searchBox']['searchButton'] = $searchButton;
        }else {
            $configuration['searchBox']['searchButton'] = 'form[role="search"] input.search-submit';

        }
        $configuration['layout'] = array();
        $configuration['layout']['navigation'] = array();
        $configuration['layout']['navigation']['position'] = 'top';
        
        $configuration['results'] = array();
        $configuration['results']['moreResultsButton'] = esc_html__('Show more results', 'site-search-360');

        if($hasContentGroups){
            $configuration['contentGroups'] = array();
            $configuration['contentGroups']['otherName'] = esc_html__('Other', 'site-search-360');
        }
        return $configuration;
    }

    public function sitesearch360CheckVersion(){
        $current_version = get_option('ss360_plugin_version');
        if($current_version==null){
            $tracker = new SiteSearch360Tracker();
            if(get_option('ss360_sr_type')!=null || get_option('ss360_account_created')!=null){ // the version was updated
                update_option('ss360_is_configured', true);
                update_option('ss360_is_indexed', true);
                update_option('ss360_old_indexing_notice', true);
                delete_option('ss360_password');
                $tracker->recordPluginEvent('07_upgrade');
            } else { // new installation
                $tracker->recordPluginEvent('install');
            }
         
            update_option('ss360_plugin_version', SITESEARCH360_VERSION);
            update_option('ss360_installation_id', uniqid());
        }
    }

    public function sitesearch360ReviewInteracted(){
        update_option('ss360_review_interaction', true);
        $res = array();
        $res['status'] = 'success';
        wp_send_json_success($res);
    }

    public function getPostCount($content_type){
        $ss360_count_posts = wp_count_posts($content_type); 
        if(!isset($ss360_count_posts->publish)){
            return 0;
        }
        $count = $ss360_count_posts->publish;
        if($count==null){
            return 0;
        }
        return $count;
    }

    public function getAllPostCount(){
        return $this->getPostCount('post') + $this->getPostCount('page') +$this->getPostCount('product');
    }

    public function getPostTypes(){
        $post_types = get_post_types( array('public' => true, '_builtin' => false, 'exclude_from_search' => false), 'names', 'and' ); 
        if($post_types == NULL){
            $post_types = array();
        }
        $post_types[] = 'post';
        $post_types[] = 'page';
        if(array_search('product', $post_types)===FALSE){
            $post_types[] = 'product';
        }
        if(array_search('scheduled-action', $post_types)!==FALSE){
            $idx = array_search('scheduled_action', $post_types);
            array_splice($post_types, $idx, 1);
        }
        if(array_search('nav_menu_item', $post_types)!==FALSE){
            $idx = array_search('nav_menu_item', $post_types);
            array_splice($post_types, $idx, 1);
        }
        return $post_types;
    }

    public function sitesearch360Track(){
        $tracker = new SiteSearch360Tracker();
        if($_POST['collection']=='intro'){
            $tracker->recordIntro($_POST['event'], isset($_POST['page']) ? ((int) $_POST['page']) : -1);
        } else if($_POST['collection']=='init'){
            if($_POST['event']=='troubleshooting'){
                $tracker->recordConfigurationDialog('search-troubleshooting', 2);
            }
        }else if($_POST['collection'] == 'cta'){
            if($_POST['type']=='plan'){
                $tracker->recordPlanCtaClick($_POST['plan'], $_POST['to']);
            }
        }else if($_POST['collection'] == 'cp'){
            $tracker-> recordControlPanelRedirect($_POST['target']);
        }
    }

    /**
     * Index up to $index_chunk_size posts/pages.
     */
    public function sitesearch360Index(){
        $index_chunk_size = 100;
        if(!empty($_POST) && isset($_POST['chunkSize'])){
            $index_chunk_size = (int) $_POST['chunkSize'];
        }
  

        $post_types = $this->getPostTypes();
        
        $posts_to_index = array();
        $posts_index_offsets = array();
        $schedule = array();

        $scheduled = 0;
        $remaining_total = 0;
        $total = 0;

        foreach($post_types as $post_type){
            $to_db_key = 'ss360_'.$post_type.'_to_index';
            $offset_db_key = 'ss360_'.$post_type.'_index_offset';
            $to_index = get_option($to_db_key);
            if($to_index==null){
                $to_index = $this->getPostCount($post_type);
                update_option($to_db_key, $to_index);
            }
            $total += $to_index;
            $posts_to_index[$post_type] = $to_index;
            $index_offset = get_option($offset_db_key);
            if($index_offset == null){
                $index_offset = 0;
            }

            $posts_index_offsets[$post_type] = $index_offset;

            $remaining = $to_index - $index_offset;
            if($remaining > 0 && $scheduled < $index_chunk_size){
                $to_schedule = max(0, min($index_chunk_size - $scheduled, $remaining));
                $scheduled +=  $to_schedule;

                $remaining_total += ($remaining - $to_schedule);

                $indexer = new SiteSearch360Indexer($post_type, $index_offset, $to_schedule);
                $indexer->index();

                $index_offset += $scheduled;
                update_option($offset_db_key, $index_offset);
            } else if($remaining > 0){
                $remaining_total += $remaining;
            }            
        }
        
        $res = array();
        $res['total'] = $total;
        $res['indexed'] = $total - $remaining_total;

        if($remaining_total <= 0){
            $ss360_tracker = new SiteSearch360Tracker();
            $ss360_tracker->recordIndexing('finish', NULL);
            update_option('ss360_is_indexed', true);
        }
        wp_send_json_success($res);
    }


    private function shouldIndex($post) {
        $post_type = get_post_type($post);
        return array_search($post_type, $this->excluded_post_types)==FALSE;
    }

    /**
     * Deletes a post from Site Search 360's search index any time the post's status transitions from 'publish' to anything else.
     *
     * @param int $new_status The new status of the post
     * @param int $old_status The old status of the post
     * @param int $post The post
     */
    public function handleTransitionPostStatus($new_status, $old_status, $post)
    {
        if ("publish" == $old_status && "publish" != $new_status && $this->shouldIndex($post)) {
            $this->deletePost($post->ID);
        }
    }

    /**
     * Index a post when its state changes from "future" to "publish"
     *
     * @param int $post The post
     */
    public function handleFutureToPublish($post)
    {
        if ("publish" == $post->post_status && $this->shouldIndex($post)) {
            $this->indexPost($post->ID);
        }
    }

    /**
     * Sends a post to the Site Search 360 API (only if post status is "publish").
     *
     * @param int $postId The id of the post to be indexed.
     */
    public function handleSavePost($postId)
    {
        $post = get_post($postId);
        if("publish" == $post->post_status && $this->shouldIndex($post)){
            $this->indexPost($postId);
        }
    }

    /**
     * Sends a request to the Site Search 360 API index a specific post in the server-side search engine.
     *
     * @param int $postId The ID of the post to be indexed.
     */
    public function indexPost($postId)
    {
        try {
            $indexer = new SiteSearch360Indexer(1,0,'');
            $indexer->indexSingle($postId);
        } catch (Error $e) {
            return;
        }
    }

    /**
     * Sends a request to the Site Search 360 API remove a specific post from the server-side search engine.
     *
     * @param int $postId The ID of the post to be deleted.
     */
    public function deletePost($postId)
    {
        $url = str_replace('__trashed','',get_permalink($postId));
        $this->client->deletePage($url);
    }

    public function getType()
    {
        $type = get_option("ss360_sr_type");
        if ($type == null) {
            return "full";
        }
        return $type;
    }

    public function getSiteId(){
        return get_option('ss360_siteId');
    }

    public function getConfig()
    {
        return json_decode(get_option("ss360_config"), true);
    }

    public function saveConfig($configObj){
        $configuration = json_encode($configObj);
        update_option('ss360_config', $configuration);
        $this->client->syncSearchDesigner();
    }

    /**
     * Override default wordpress search
     * @param $posts
     * @param $query
     */
    public function overrideSearch($posts, $query = false)
    {
        if (!$query)
            return $posts;

        if (!$query->is_search())
            return $posts;

        if (!$query->is_main_query())
            return $posts;

        if ($query->is_paged())
            return $posts;

        if($this->getType()=="full")
            return $posts;

        $searchterm = trim($_GET['s']);

        if (empty($searchterm)) {
            return array();
        }

        $limit = 60;
        $log = true;

        if ($_GET['ajax'] == 1) {
            $limit = 6;
            $log = false;
        }

        try {
            $results = $this->client->search($searchterm, $limit, $log);
        } catch (Error $e) {
            return $posts;
        }
 
        $totalResults = $results['totalResults'];
        if ($totalResults == 0) {
            return array();
        }

        $suggests = $results['suggests']['_'];
        $ids = array();
        if (is_array($suggests) || is_object($suggests)) {
            foreach ($suggests as &$suggests) {
                $permalink = $suggests['link'];
                $ids[] = url_to_postid($permalink);
            }
        }
        else{
            return array();
        }

        if (!count($ids))
            return array();
        return get_posts(array('include' => $ids, 'orderby' => 'post__in', 'post_type'=>'any', 'post_status' => array('Import List', 'publish')));
    }

    /**
     * Includes the Site Search 360 Search plugin's admin page
     */
    public function sitesearch360AdminPage()
    {
        include('sitesearch360-admin-page.php');
    }

    public function sitesearch360BaseConfigPage(){
        include('sitesearch360-configuration-basics.php');
    }

    public function sitesearch360SearchResultPage(){
        include('sitesearch360-configuration-search-results.php');
    }

    public function sitesearch360SuggestionPage(){
        include('sitesearch360-configuration-suggestions.php');
    }

    public function sitesearch360FilterPage(){
        include('sitesearch360-configuration-filters.php');
    }

    public function sitesearch360EditorPage(){
        include('sitesearch360-editor-page.php');
    }

    public function siteSearch360SearchDesignerPage(){
        include('sitesearch360-search-designer.php');
    }

    /**
     * Create a menu in the WordPress admin for the Site Search 360 Search plugin.
     */
    public function sitesearch360Menu()
    {
        add_menu_page('Site Search 360', 'Site Search 360', 'manage_options', "sitesearch360", array($this, 'sitesearch360AdminPage'), plugins_url('assets/ss360_logo_menu.png', __FILE__));
        $baseConfigLabel = esc_html__('Basic Configuration', 'site-search-360');
        $searchResultsLabel = esc_html__('Search Results', 'site-search-360');
        $suggestionsLabel = esc_html__('Suggestions', 'site-search-360');
        $filterLabel = esc_html__('Filters', 'site-search-360');
        $editorLabel = esc_html__('Configuration Editor', 'site-search-360');
        $searchDesignerLabel = esc_html__('Search Designer', 'site-search-360');
        $is_configured = get_option("ss360_is_configured");
        $integration_type = $this->getType();
        if($is_configured==null && !empty($_POST) && isset($_POST['page'])){ // configuration finished, but option not updated yet
            $page = $_POST['page'];
            $is_configured = ($page > 2 && $integration_type=='filter') || ($page > 3);
        }
        if(!empty($_POST) && isset($_POST['action']) && isset($_POST['_wpnonce']) && $_POST['action']=='ss360_setType' && isset($_POST['ss360_sr_type'])){ // updating search result type
            $integration_type = $_POST['ss360_sr_type'];
        }
        add_submenu_page('sitesearch360', 'Site Search 360', esc_html__('Dashboard', 'site-search-360'), 'manage_options', 'sitesearch360', array($this, 'sitesearch360AdminPage'));
        if($is_configured){
            if($integration_type!='filter'){
                add_submenu_page('sitesearch360', $baseConfigLabel.' - Site Search 360', $baseConfigLabel, 'manage_options', 'sitesearch360-basic-configuration', array($this, 'sitesearch360BaseConfigPage'));
            }
            if($integration_type=='full'){
                add_submenu_page('sitesearch360', $searchResultsLabel.' - Site Search 360', $searchResultsLabel, 'manage_options','sitesearch360-search-results', array($this, 'sitesearch360SearchResultPage'));
            }
            if($integration_type!='filter'){
                add_submenu_page('sitesearch360', $suggestionsLabel.' - Site Search 360', $suggestionsLabel, 'manage_options','sitesearch360-suggestions', array($this, 'sitesearch360SuggestionPage'));
            }
            $current_plan = get_option('ss360_active_plan');
            if($integration_type=='full' && $current_plan!='FREE' && $current_plan!='COLUMBO'){
                add_submenu_page('sitesearch360', $filterLabel.' - Site Search 360', $filterLabel, 'manage_options','sitesearch360-filter', array($this, 'sitesearch360FilterPage'));
            }
            if($integration_type!='filter'){
                add_submenu_page('sitesearch360', $editorLabel.' - Site Search 360', $editorLabel, 'manage_options','sitesearch360-editor', array($this, 'sitesearch360EditorPage'));
                add_submenu_page('sitesearch360', $searchDesignerLabel.' - Site Search 360', $searchDesignerLabel, 'manage_options','sitesearch360-search-designer', array($this, 'siteSearch360SearchDesignerPage'));
            }
        }
    }

    /**
     * Enqueues the styles used by the plugin's admin page.
     * This method is called by the admin_enqueue_scripts action.
     */
    public function enqueue_admin_assets($hook)
    {
        if (strpos($hook, 'sitesearch360') == false)
            return;
        wp_enqueue_style('admin_styles', plugins_url('assets/ss360_admin_styles.min.css', __FILE__));
    }

    /**
     * Enqueues the javascripts and styles to be used by the plugin on the primary website.
     * This method is called by the wp_enqueue_scripts action.
     */
    public function enqueueSitesearch360Assets()
    {
        if (!function_exists('ss360Config'))   {
            function ss360Config()
            {
                $plugin = new SiteSearch360Plugin();
                $type = $plugin->getType();
                if ($type != "filter") {
                    $configuration = $plugin->getConfig();
                    if($type!='full'){
                        if(isset($configuration['searchBox'])){unset($configuration['searchBox']['searchButton']);}
                        if(isset($configuration['voiceSearch'])){unset($configuration['voiceSearch']['enabled']);}
                    }

                    ?>
                    <script type="text/javascript">
                        var ss360Config = <?php echo json_encode($configuration); ?>;
                    </script>
                    <script type="text/javascript">
                        if (ss360Config.callbacks === undefined) {
                            ss360Config.callbacks = {};
                        }
                        ss360Config.callbacks.preSearch = function (selectedText) {
                            if (selectedText === undefined || selectedText.length === 0) {
                                return false;
                            }
                            return true;
                        }
                    </script>
                    <?php if ($type == 'suggestions') { ?>
                        <script type="text/javascript">
                            ss360Config.callbacks.preSearch = function (selectedText, sort, selectedSearchBox) {
                                selectedSearchBox.parents("form")[0].submit();
                                return false;
                            }
                        </script>
                    <?php } ?>
                    <script src="https://cdn.sitesearch360.com/sitesearch360-v12.min.js" async></script>
                <?php } ?>
                <?php
            }
        }
        if(get_option('ss360_account_created')){
            add_action('wp_footer', 'ss360Config');
        }
    }

    static function on_activate(){
        require_once 'class-sitesearch360-tracker.php';
        $tracker = new SiteSearch360Tracker();
        $tracker->recordPluginEvent('activate');    
    }

    static function on_deactivate(){
        require_once 'class-sitesearch360-tracker.php';
        $tracker = new SiteSearch360Tracker();
        $tracker->recordPluginEvent('deactivate');    
    }
}

register_activation_hook(__FILE__, array('SiteSearch360Plugin', 'on_activate'));
register_deactivation_hook(__FILE__, array('SiteSearch360Plugin', 'on_deactivate'));