<?php

class SiteSearch360Tracker {

    private $WRITE_KEY = 'A5C4318CFCDA5F73E4EB70B13D6BE8D4C8391FD00FEC5C6398DF924A5751C713593B4577009AF250A2BEEED4F58497E1352200FE518722A5DC7585311FFD46CFC96954B3BA14D8407A37D454E414AB1CDD8D8CE53F829BB31876F25C43BA6642';
    private $PROJECT_ID = '5c77e9b3c9e77c0001eddd31';

    private $siteId = NULL;
    private $installationId = NULL;

    public function __construct(){
        $this->siteId = get_option('ss360_siteId');
        $this->installationId = get_option('ss360_installation_id');
    }

    public function recordPluginEvent($event_type){
        $this->track('plugin', array('type'=>$event_type));
    }

    public function recordIntro($event_type, $page){
        $data = array('type'=>$event_type);
        if($page>0 && $event_type=='skip'){
            $data['page'] = $page;
        }
        $this->track('intro', $data);
    }

    public function recordConfigurationDialog($event_type, $page){ 
        $this->track('init', array('type'=>$event_type, 'page'=>$page));
    }

    public function recordPersonalization($changed){
        $this->track('personalization', array('modified'=>$changed, 'type'=>'finish'));
    }

    public function recordIntegrationChange($type){
        $this->track('integration', array('type'=>'change', 'integration'=>$type));
    }

    public function recordImpression($page){
        $this->track('impression', array('type'=>'impression', 'page'=>$page));
    }

    public function recordConfigChange($source){
        $this->track('config', array('type'=>'update', 'source'=>$source));
    }

    public function recordPlanCtaClick($current_plan, $new_plan){
        $this->track('cta', array('type'=>'plan', 'plan'=>array('from'=>$current_plan, 'to'=>$new_plan)));
    }

    public function recordControlPanelRedirect($target){
        $this->track('control-panel', array('type'=>'follow', 'target'=>$target));
    }

    public function recordAccountEvent($type){
        $this->track('account', array('type'=>$type));
    }

    public function recordIndexing($event_type, $trigger){
        $data = array('type'=>$event_type);
        if($trigger!=null){
            $data['trigger'] = $trigger;
        }
        $this->track('indexing', $data);
    }

    private function track($collection, $data){
        $url = 'https://api.keen.io/3.0/projects/'.$this->PROJECT_ID.'/events/'.$collection;

        $headers = array(
            'User-Agent' => 'Site Search 360 Wordpress Plugin/' . SITESEARCH360_VERSION,
            'Authorization' => $this->WRITE_KEY,
            'Content-Type' => 'application/json'
        );

        if($this->siteId!=NULL){
            $data['siteId'] = $this->siteId;
        }

        if($this->installationId!=NULL){
            $data['installationId'] = $this->installationId;
        }

        $data['version'] = defined('SITESEARCH360_VERSION') ? SITESEARCH360_VERSION : '1.0.30'; //TODO: update me frequently :)

		$args = array(
			'method' => 'POST',
			'headers' => $headers,
			'timeout' => 10,
			'redirection' => 2,
			'httpversion' => '1.0',
			'blocking' => false,
			'body' => null,
            'cookies' => array(),
            'body' => json_encode($data)
        );
		
		$response = wp_remote_request( $url, $args );
    }
}